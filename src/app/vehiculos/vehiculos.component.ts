import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Vehiculo } from './vehiculo.model';
import { VehiculosService } from './vehiculos.service';

@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {
  listaVehiculos: Vehiculo[] = [];

  constructor(private veService: VehiculosService,
    private router:Router) { }

  ngOnInit(): void {
    this.cargarVehiculos();
  }

  cargarVehiculos() {
    //this.listaVehiculos = this.veService.getVehiculos();
    this.veService.getVehiculos()
      .subscribe(lista => {
        console.log('lista recibida');
        this.listaVehiculos = lista;
      });
      // OJO SE EJECUTA ANTES DE RECIBIR LA LISTA
      console.log(this.listaVehiculos);
  }

  modificarVehiculo(placas: string) {
    console.log('editar', placas);
    this.router.navigate(['/vehiculos','edit',placas]);
  }

  eliminarVehiculo(placas:string) {
    if (!confirm('¿Está seguro que desea eliminar el vehículo ' + placas + '?')) {
      return;
    }
    this.veService.eliminarVehiculo(placas)
      .subscribe({
        next: res =>{
          //this.cargarVehiculos();
          for (let ii = 0; ii < this.listaVehiculos.length; ii++) {
            if (this.listaVehiculos[ii].placas === placas) {
              this.listaVehiculos.splice(ii, 1)
            }
          }
        },
        error: err => { console.log(err)}
      });

  }
}
