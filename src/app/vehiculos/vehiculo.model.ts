import { Cliente } from "./cliente.model";

export interface Vehiculo {
  placas: string,
  marca: string,
  modelo: string,
  color?: string,
  cliente? : Cliente
}
