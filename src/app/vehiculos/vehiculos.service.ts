import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { Vehiculo } from "./vehiculo.model";
import { environment } from "src/environments/environment";

@Injectable({ providedIn:'root'})
export class VehiculosService {
  /*
  private listaVehiculos: Vehiculo[] = [
    {
      placas:'QWE8712',
      marca:'Ford',
      modelo:'Focus',
      color:'Rojo'
    },
    {
      placas:'POW8235',
      marca:'Chrysler',
      modelo:'123',
      color:'Azul'
    }
  ];*/
  BASE_URL: string = environment.BACKEND_BASE_URL;

  constructor(private http:HttpClient) {}

  getVehiculos() : Observable<Vehiculo[]> {
    //return of(this.listaVehiculos);
    return this.http.get<Vehiculo[]>( this.BASE_URL + '/vehiculos')
  }

  getVehiculoPorPlacas(placas: string) : Observable<Vehiculo> {
    return this.http.get<Vehiculo>( this.BASE_URL + '/vehiculos/' + placas)
  }

  guardarVehiculo(vehiculo: Vehiculo, edicion: boolean) : Observable<Vehiculo> {
    if (edicion) {
      return this.http
        .put<Vehiculo>(this.BASE_URL + '/vehiculos/' + vehiculo.placas, vehiculo)
    }
    else {
      return this.http.post<Vehiculo>(this.BASE_URL + '/vehiculos', vehiculo);
    }
  }

  eliminarVehiculo(placas: string): Observable<void> {
    return this.http.delete<void>(this.BASE_URL + '/vehiculos/' + placas)
  }
}
