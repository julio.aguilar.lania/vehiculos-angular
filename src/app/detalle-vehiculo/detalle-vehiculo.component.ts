import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Vehiculo } from '../vehiculos/vehiculo.model';
import { VehiculosService } from '../vehiculos/vehiculos.service';

@Component({
  selector: 'app-detalle-vehiculo',
  templateUrl: './detalle-vehiculo.component.html',
  styleUrls: ['./detalle-vehiculo.component.css']
})
export class DetalleVehiculoComponent implements OnInit {

  vehiculo: Vehiculo = { placas:'', modelo:'', marca:'', color:''};
  fechaRegistro: Date | any;

  constructor(private route:ActivatedRoute,
    private veService: VehiculosService) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      let id = params['id'];
      this.veService.getVehiculoPorPlacas(id)
        .subscribe(v => {this.vehiculo = v})
    });
  }
}
