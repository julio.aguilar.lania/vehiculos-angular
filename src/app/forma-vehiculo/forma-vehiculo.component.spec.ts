import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormaVehiculoComponent } from './forma-vehiculo.component';

describe('FormaVehiculoComponent', () => {
  let component: FormaVehiculoComponent;
  let fixture: ComponentFixture<FormaVehiculoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormaVehiculoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormaVehiculoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
