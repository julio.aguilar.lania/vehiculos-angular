import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cliente } from '../vehiculos/cliente.model';
import { Vehiculo } from '../vehiculos/vehiculo.model';
import { ClientesService } from '../vehiculos/clientes.service';
import { VehiculosService } from '../vehiculos/vehiculos.service';

@Component({
  selector: 'app-forma-vehiculo',
  templateUrl: './forma-vehiculo.component.html',
  styleUrls: ['./forma-vehiculo.component.css']
})
export class FormaVehiculoComponent implements OnInit {

  nuevoVehiculo: Vehiculo = {placas:'', marca:'', modelo:'', color:''}
  mensaje = "";
  mensajeVisible = false;
  listaClientes: Cliente[] = [];
  idClienteSeleccionado: number = -1;
  modoEdicion:boolean = false;

  constructor(private veService: VehiculosService,
    private clService: ClientesService,
    private router: Router,
    private ruta:ActivatedRoute){}

    ngOnInit(): void {
      this.ruta.params.subscribe(pars => {
        if (pars['id']) {
          this.modoEdicion = true;
          this.veService.getVehiculoPorPlacas(pars['id'])
            .subscribe(v => {
              this.nuevoVehiculo = v;
              // Opcional para cuando queremos usar el select
              if (this.nuevoVehiculo.cliente) {
                this.idClienteSeleccionado = this.nuevoVehiculo.cliente.clienteId;
              }
            })
        }
      })
      this.clService.getClientes()
        .subscribe(lista => {this.listaClientes = lista})
    }

  guardarVehiculo() {
    this.nuevoVehiculo.cliente = { clienteId: this.idClienteSeleccionado};
    this.veService.guardarVehiculo(this.nuevoVehiculo, this.modoEdicion)
      .subscribe(
        {
          next:v =>{
            console.log('Guardado con éxito');
            /*
            this.nuevoVehiculo = {placas:'', marca:'', modelo:'', color:''}
            this.mensaje = 'Guardado con éxito';
            this.mensajeVisible = true;
            */
           this.router.navigate(['/vehiculos']);
          },
          error:err => {console.log('ERROR', err);}
        });
  }
}
