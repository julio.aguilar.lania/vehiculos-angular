import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { DetalleVehiculoComponent } from './detalle-vehiculo/detalle-vehiculo.component';
import { FormaVehiculoComponent } from './forma-vehiculo/forma-vehiculo.component';
import { VehiculosComponent } from './vehiculos/vehiculos.component';

const routes: Routes = [
  {
    path: 'home',
    component: AppComponent // TODO: Cambiar por un HomeComponent
  },
  {
    path: 'vehiculos',
    component: VehiculosComponent
  },
  {
    path: 'vehiculos/create',
    component: FormaVehiculoComponent
  },
  {
    path: 'vehiculos/:id',
    component: DetalleVehiculoComponent
  },
  {
    path: 'vehiculos/edit/:id',
    component: FormaVehiculoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
